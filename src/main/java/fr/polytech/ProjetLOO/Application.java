package fr.polytech.ProjetLOO;

import fr.polytech.ProjetLOO.Controller.ClientController;
import fr.polytech.ProjetLOO.Controller.ContratController;
import fr.polytech.ProjetLOO.Controller.MeubleController;
import fr.polytech.ProjetLOO.DAO.ClientDAO;
import fr.polytech.ProjetLOO.DAO.ContratDAO;
import fr.polytech.ProjetLOO.DAO.MeubleDAO;
import fr.polytech.ProjetLOO.DAO.SQLConnector;
import fr.polytech.ProjetLOO.Model.*;
import fr.polytech.ProjetLOO.View.ClientView;
import fr.polytech.ProjetLOO.View.ContratView;
import fr.polytech.ProjetLOO.View.MeubleView;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        ContratController contratController = new ContratController(new ContratView());
        ClientController clientController = new ClientController(new ClientView());
        MeubleController meubleController = new MeubleController(new MeubleView());

        Scanner scanner = new Scanner(System.in);

        boolean exit = false;

        while(!exit) {
            System.out.println("\n ------------------- Choisissez une opération ------------------- ");
            System.out.println("1: afficher les clients");
            System.out.println("2: afficher les contrats d'un client");
            System.out.println("3: inserez un nouveau contrat");
            System.out.println("4: afficher les meubles ayant une hauteur donnée");
            System.out.println("exit: quitter l'application");
            System.out.print("choix : ");
            String res = scanner.nextLine();
            switch (res) {
                case "1" -> clientController.showAllClients();
                case "2" -> {
                    System.out.print("Entrez l'id du client : ");
                    int idRes = scanner.nextInt();
                    scanner.nextLine();
                    contratController.showContratFromClient(idRes);
                }
                case "3" -> contratController.showInsertContrat();
                case "4" -> {
                    System.out.print("Entrez la hauteur : ");
                    float hauteur = scanner.nextFloat();
                    scanner.nextLine();
                    meubleController.showMeublesWithHeight(hauteur);
                }
                case "exit" -> {
                    System.out.println("Au revoir");
                    exit = true;
                }
                default -> System.out.println("opération non reconnu.");
            }
        }
    }
}
