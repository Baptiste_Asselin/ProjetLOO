package fr.polytech.ProjetLOO.View;

import fr.polytech.ProjetLOO.Model.Contrat;
import fr.polytech.ProjetLOO.Model.Livraison;
import fr.polytech.ProjetLOO.Model.Meuble_livree;
import fr.polytech.ProjetLOO.Model.Meuble_Contrat;

import java.util.List;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ContratView {
    public void displayContrat(List<Contrat> contrats) {
        System.out.println("\nContrat du client : " + contrats.get(1).getIdClient());
        for (Contrat contrat : contrats) {
            System.out.println();
            System.out.println("Date de conclusion du contrat : " + contrat.getDate_conclusion());
            System.out.println("Vendu par : " + contrat.getIdEmploye());
            System.out.println("Description du contrat : " + contrat.getDescription());
            System.out.println("Prix Total : " + contrat.getPrix() + "€");
            System.out.println("Livraison : ");
            System.out.println("    Adresse de livraison : " + contrat.getAdresse_livraison());
            for (Livraison livraison : contrat.getLivraisons()) {
                System.out.println("    Date livraison : " + livraison.getDate_livraison().toString());
                System.out.println("    Heure de livraison : " + livraison.getHeure_livraison().toString());
                System.out.println("    Meubles :");
                for (Meuble_livree meuble : livraison.getMeubles()) {
                    System.out.println("        Code du meuble : " + meuble.getCode() + ", quantite livree : " + meuble.getQuantite());
                }
            }
        }
    }

    public Contrat displayContratForm() {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Entrez la date du contrat (jj/mm/aaaa) : ");
        LocalDate date = LocalDate.parse(scanner.nextLine(), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        System.out.print("Entrez l'adresse de livraison : ");
        String address = scanner.nextLine();
        System.out.print("Entrez une description: ");
        String desc = scanner.nextLine();
        System.out.print("Entrez le prix de la commande: ");
        float prix = scanner.nextFloat();
        scanner.nextLine();
        System.out.print("Entrez l'identifiant du client : ");
        int idClient = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Entrez l'identifiant du vendeur : ");
        int idVendeur = scanner.nextInt();
        scanner.nextLine();

        Contrat res = new Contrat(0, date, address, desc, prix);
        res.setIdEmploye(idVendeur);
        res.setIdClient(idClient);

        System.out.println("------- MEUBLES -------\n");
        System.out.print("Entrez le nombre de type de meuble : ");
        int nbMeubleType = scanner.nextInt();
        scanner.nextLine();
        List<Meuble_Contrat> meuble_contrats = new ArrayList<>(nbMeubleType);

        for(int i = 0; i < nbMeubleType; i++) {
            System.out.println("Meuble " + (i+1) + " :");
            System.out.print("Entrez le code du type de meuble : ");
            String code = scanner.nextLine();
            System.out.print("Entrez le nombre de meuble de ce type : ");
            int nbMeuble = scanner.nextInt();
            scanner.nextLine();

            meuble_contrats.add(new Meuble_Contrat(code, nbMeuble,0));
        }

        System.out.println("------- LIVRAISONS -------\n");
        System.out.print("Entrez le nombre de livraisons: ");
        int nbLivraison = scanner.nextInt();
        scanner.nextLine();
        List<Livraison> livraisons = new ArrayList<>(nbLivraison);

        List<Meuble_Contrat> meuble_contratsNotDelivered = new ArrayList<>(meuble_contrats.size()); // furniture not in a delivery order
        for(Meuble_Contrat meuble : meuble_contrats) {
            meuble_contratsNotDelivered.add(new Meuble_Contrat(meuble.getCode(), meuble.getQuantite(), 0));
        }

        for(int i = 0; i < nbLivraison-1; i++) {
            System.out.println("Livraison " + (i+1) + " :");
            System.out.print("Entrez la date de la livraison (jj/mm/aaaa) : ");
            LocalDate dateLiv = LocalDate.parse(scanner.nextLine(), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
            System.out.print("Entrez l'heure de la livraison (hh:mm) : ");
            LocalTime timeLiv = LocalTime.parse(scanner.nextLine(), DateTimeFormatter.ofPattern("HH:mm"));

            livraisons.add(new Livraison(0, dateLiv, timeLiv));

            System.out.print("Entrez le nombre de type de meuble (max : " + meuble_contratsNotDelivered.size() + ") : ");
            int nbMeubleTypeLiv = scanner.nextInt();
            scanner.nextLine();

            List<Meuble_livree> meubleLivrees = new ArrayList<>(nbMeubleTypeLiv);

            for(int j = 0; j < nbMeubleTypeLiv; j++) {
                System.out.println("Meuble " + (j+1) + " :");
                System.out.println("Choix disponible:");
                displayChoices(meuble_contratsNotDelivered);
                System.out.print("Entrez le code du meuble : ");
                String code = scanner.nextLine();
                System.out.print("Entrez la quantité : ");
                int qte = scanner.nextInt();
                scanner.nextLine();

                meubleLivrees.add(new Meuble_livree(code ,0, qte));

                meuble_contratsNotDelivered.forEach(elem -> {
                    if(elem.getCode().equals(code)) {
                        elem.setQuantite(elem.getQuantite() - qte);
                    }
                });
                meuble_contratsNotDelivered = meuble_contratsNotDelivered.stream().filter(elem -> elem.getQuantite() > 0).toList();

                livraisons.get(i).setMeubles(meubleLivrees);
            }
        }

        System.out.println("Livraison " + nbLivraison + " :");
        System.out.print("Entrez la date de la livraison (jj/mm/aaaa) : ");
        LocalDate dateLiv = LocalDate.parse(scanner.nextLine(), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        System.out.print("Entrez l'heure de la livraison (hh:mm) : ");
        LocalTime timeLiv = LocalTime.parse(scanner.nextLine(), DateTimeFormatter.ofPattern("HH:mm"));

        livraisons.add(new Livraison(0, dateLiv, timeLiv));

        System.out.println("Le reste des meuble sera ajouté à la dernière livraison :");
        displayChoices(meuble_contratsNotDelivered);

        List<Meuble_livree> meubleLivrees = new ArrayList<>(meuble_contratsNotDelivered.size());

        for(Meuble_Contrat meuble : meuble_contratsNotDelivered) {
            meubleLivrees.add(new Meuble_livree(meuble.getCode(), 0, meuble.getQuantite()));
        }

        livraisons.get(nbLivraison-1).setMeubles(meubleLivrees);

        res.setMeubles(meuble_contrats);
        res.setLivraisons(livraisons);

        return res;
    }
    private void displayChoices(List<Meuble_Contrat> choices) {
        for(Meuble_Contrat choice : choices) {
            System.out.println("code : " + choice.getCode() + " , Qte : " + choice.getQuantite());
        }
    }
}
