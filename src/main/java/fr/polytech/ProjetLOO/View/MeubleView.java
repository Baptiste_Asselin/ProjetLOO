package fr.polytech.ProjetLOO.View;

import fr.polytech.ProjetLOO.Model.Meuble;

import java.util.List;

public class MeubleView {
    public void displayMeuble(List<Meuble> meubles) {
        System.out.println("Meubles :");
        for(Meuble meuble : meubles) {
            System.out.println("Code : " + meuble.getCode());
            System.out.println("Dimension : " + meuble.getHauteur() + "x" + meuble.getLargeur() + "x" + meuble.getProfondeur());
            System.out.println("Prix : " + meuble.getPrix() + " euros");
        }
    }

}
