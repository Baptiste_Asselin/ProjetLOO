package fr.polytech.ProjetLOO.View;

import fr.polytech.ProjetLOO.Model.ClientPrive;
import fr.polytech.ProjetLOO.Model.Entreprise;

import java.util.List;

public class ClientView {

    public void displayClientPrive(List<ClientPrive> clients) {
        System.out.println("\nClients privés :");
        for(ClientPrive client: clients) {
            System.out.println();
            System.out.println("Nom : " + client.getName());
            System.out.println("Prénom : " + client.getFirstname());
            System.out.println("Adresse : " + client.getAdresse());
            System.out.println("Télépone : " + client.getPhone());
        }
    }

    public void displayEntreprise(List<Entreprise> entreprises) {
        System.out.println("\nEntreprises :");
        for(Entreprise entreprise : entreprises) {
            System.out.println();
            System.out.println("Nom : " + entreprise.getName());
            System.out.println("Adresse : " + entreprise.getAddress());
            System.out.println("Téléphones :");
            List<String> numbers = entreprise.getNumber();
            for(String number : numbers) {
                System.out.println("\t" + number);
            }
        }
    }
}
