package fr.polytech.ProjetLOO.Controller;

import fr.polytech.ProjetLOO.DAO.ClientDAO;
import fr.polytech.ProjetLOO.Model.ClientPrive;
import fr.polytech.ProjetLOO.Model.Entreprise;
import fr.polytech.ProjetLOO.View.ClientView;

import java.util.List;

public class ClientController {

    private ClientView view;

    public ClientController(ClientView cv) {
        view = cv;
    }
    public void showAllClients() {
        List<ClientPrive> clientPrives = ClientDAO.getAllClientPrive();
        if(clientPrives != null) {
            view.displayClientPrive(clientPrives);
        }
        List<Entreprise> entreprises = ClientDAO.getAllEntreprise();
        if(entreprises != null) {
            view.displayEntreprise(entreprises);
        }
    }
}
