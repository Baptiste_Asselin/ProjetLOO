package fr.polytech.ProjetLOO.Controller;

import fr.polytech.ProjetLOO.DAO.ContratDAO;
import fr.polytech.ProjetLOO.Model.Contrat;
import fr.polytech.ProjetLOO.View.ContratView;
import java.util.List;

public class ContratController {
    private ContratView view;

    public ContratController(ContratView cv) { view = cv;}

    public void showContratFromClient(int idClient) {
        List<Contrat> contrats = ContratDAO.getContratFromClient(idClient);
        if (contrats != null) {
            view.displayContrat(contrats);
        }
    }

    public void showInsertContrat() {
        Contrat contrat = view.displayContratForm();
        ContratDAO.insertContract(contrat);
    }
}
