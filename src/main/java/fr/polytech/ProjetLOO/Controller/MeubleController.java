package fr.polytech.ProjetLOO.Controller;

import fr.polytech.ProjetLOO.DAO.MeubleDAO;
import fr.polytech.ProjetLOO.Model.Meuble;
import fr.polytech.ProjetLOO.View.MeubleView;

import java.util.List;

public class MeubleController {

    private MeubleView view;

    public MeubleController(MeubleView mv) {
        view = mv;
    }

    public void showMeublesWithHeight(float height) {
        List<Meuble> meubles = MeubleDAO.getMeubleWithHeight(height);
        view.displayMeuble(meubles);
    }
}
