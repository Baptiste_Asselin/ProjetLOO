package fr.polytech.ProjetLOO.DAO;

import fr.polytech.ProjetLOO.Model.*;

import javax.xml.transform.Result;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class ContratDAO {
    public static List<Contrat> getContratFromClient(int idclient) {
        ResultSet rs;
        List<Contrat> result = new LinkedList<Contrat>();
        SQLConnector connector = SQLConnector.getInstance();
        PreparedStatement ps = connector.getPreparedStatement("SELECT c.id_contrat , c.date_conclusion , c.adresse_livraison , c.description , c.prix_total, c.id_client, c.id_employe FROM Contrat AS c WHERE c.id_client = ?;");
        try {
            ps.setInt(1 , idclient);
            rs = ps.executeQuery();
            while (rs.next()){
                Contrat contrat = new Contrat(rs.getInt("id_contrat") , rs.getDate("c.date_conclusion").toLocalDate(), rs.getString("c.adresse_livraison") , rs.getString("c.description") , rs.getFloat("c.prix_total"));
                contrat.setIdClient(rs.getInt("c.id_client"));
                contrat.setIdEmploye(rs.getInt("c.id_employe"));
                PreparedStatement psm = connector.getPreparedStatement("select code , quantite from Meuble_Contrat where id_contrat = ? ;");
                psm.setInt(1 , contrat.getId());
                ResultSet rsm = psm.executeQuery();
                while (rsm.next()){
                    contrat.addMeuble(new Meuble_Contrat(rsm.getString(1) , rsm.getInt(2) , contrat.getId()));
                }
                PreparedStatement psl = connector.getPreparedStatement("select * from Livraison where id_contrat = ? ;");
                psl.setInt(1 , contrat.getId());
                ResultSet rsl = psl.executeQuery();
                while (rsl.next()){
                    Livraison livraison = new Livraison(rsl.getInt("id_livraison") , rsl.getDate("date_livraison").toLocalDate(),rsl.getTime("heure_livraison").toLocalTime());
                    PreparedStatement psml = connector.getPreparedStatement("SELECT code , quantite from Meuble_livree where id_livraison = ?");
                    psml.setInt(1 ,rsl.getInt("id_livraison"));
                    ResultSet rsml = psml.executeQuery();
                    while(rsml.next()){
                        livraison.addMeuble(new Meuble_livree(rsml.getString(1) , rsl.getInt("id_livraison") , rsml.getInt(2)));
                    }
                    contrat.addLivraison(livraison);
                }
                result.add(contrat);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    public static void insertContract(Contrat contrat) {
        SQLConnector connector = SQLConnector.getInstance();

        PreparedStatement psContrat = connector.getPreparedStatement("INSERT INTO Contrat(date_conclusion, adresse_livraison, description, prix_total, id_client, id_employe) values (?,?,?,?,?,?);");

        PreparedStatement psLivraison = connector.getPreparedStatement("INSERT INTO Livraison(date_livraison, heure_livraison, id_contrat) values (?,?,?);");

        PreparedStatement psMeubleLivree = connector.getPreparedStatement("INSERT INTO Meuble_livree(code, id_livraison, quantite) values(?,?,?);");

        PreparedStatement psMeubleContrat = connector.getPreparedStatement("INSERT INTO Meuble_Contrat(code, id_contrat, quantite) values(?,?,?);");

        int idContrat;

        try {
            psContrat.setDate(1, Date.valueOf(contrat.getDate_conclusion()));
            psContrat.setString(2, contrat.getAdresse_livraison());
            psContrat.setString(3, contrat.getDescription());
            psContrat.setFloat(4, contrat.getPrix());
            psContrat.setInt(5, contrat.getIdClient());
            psContrat.setInt(6, contrat.getIdEmploye());
            psContrat.execute();
            ResultSet rsContrat = psContrat.getGeneratedKeys();
            rsContrat.first();
            idContrat = rsContrat.getInt(1);
            rsContrat.close();

            for(Livraison livraison : contrat.getLivraisons()) {
                psLivraison.setDate(1, Date.valueOf(livraison.getDate_livraison()));
                psLivraison.setTime(2, Time.valueOf(livraison.getHeure_livraison()));
                psLivraison.setInt(3, idContrat);

                psLivraison.execute();
                int idLivraison;
                ResultSet rsLivraison = psLivraison.getGeneratedKeys();
                rsLivraison.first();
                idLivraison = rsLivraison.getInt(1);
                rsLivraison.close();
                for(Meuble_livree meuble : livraison.getMeubles()) {
                    psMeubleLivree.setString(1, meuble.getCode());
                    psMeubleLivree.setInt(2, idLivraison);
                    psMeubleLivree.setInt(3, meuble.getQuantite());
                    psMeubleLivree.execute();
                }
            }

            for(Meuble_Contrat meuble_contrat: contrat.getMeubles()) {
                psMeubleContrat.setString(1, meuble_contrat.getCode());
                psMeubleContrat.setInt(2, idContrat);
                psMeubleContrat.setInt(3, meuble_contrat.getQuantite());
                psMeubleContrat.execute();
            }
            connector.closeConnection();
        } catch(SQLException se) {
            se.printStackTrace();
        }

    }
}
