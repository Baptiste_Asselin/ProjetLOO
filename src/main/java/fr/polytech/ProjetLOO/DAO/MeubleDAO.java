package fr.polytech.ProjetLOO.DAO;

import fr.polytech.ProjetLOO.Model.Meuble;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class MeubleDAO {
    public static List<Meuble> getMeubleWithHeight(float height){
        ResultSet rs;
        List<Meuble> result = new LinkedList<Meuble>();
        SQLConnector connector = SQLConnector.getInstance();
        PreparedStatement ps = connector.getPreparedStatement("Select * from Meuble where hauteur = ?");
        try {
            ps.setFloat(1 , height);
            rs = ps.executeQuery();
            while(rs.next()){
                result.add(new Meuble(rs.getString("code") , rs.getFloat("prix") , rs.getFloat("hauteur") , rs.getFloat("largeur") , rs.getFloat("profondeur")));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return result;
    }
}
