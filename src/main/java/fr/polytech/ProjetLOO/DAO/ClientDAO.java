package fr.polytech.ProjetLOO.DAO;

import fr.polytech.ProjetLOO.Model.ClientPrive;
import fr.polytech.ProjetLOO.Model.Entreprise;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClientDAO {

    public static List<ClientPrive> getAllClientPrive() {
        SQLConnector connector = SQLConnector.getInstance();

        List<ClientPrive> res = new ArrayList<>();

        PreparedStatement ps = connector.getPreparedStatement("SELECT nom, prenom, telephone, adresse FROM Client_prive;");

        try {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ClientPrive cur = new ClientPrive(rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4));
                res.add(cur);
            }
            connector.closeConnection();
            return res;
        } catch(SQLException se) {
            se.printStackTrace();
        }

        return null;
    }

    public static List<Entreprise> getAllEntreprise() {
        SQLConnector connector = SQLConnector.getInstance();

        List<Entreprise> res = new ArrayList<>();

        PreparedStatement ps = connector.getPreparedStatement("SELECT id_cLient, nom, adresse FROM Entreprise;");

        PreparedStatement psNum = connector.getPreparedStatement("SELECT num FROM Numero_telephone WHERE id_client = ?");
        try {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Entreprise cur = new Entreprise(rs.getString(2), rs.getString(3));
                psNum.setInt(1, rs.getInt(1));
                ResultSet rsNum = psNum.executeQuery();
                List<String> numbers = new ArrayList<>();
                while(rsNum.next()) {
                    numbers.add(rsNum.getString(1));
                }
                rsNum.close();
                cur.setNumber(numbers);
                res.add(cur);
            }
            connector.closeConnection();
            return res;
        } catch(SQLException se) {
            se.printStackTrace();
        }

        return null;
    }
}
