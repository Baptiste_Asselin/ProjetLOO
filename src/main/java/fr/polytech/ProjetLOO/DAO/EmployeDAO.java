package fr.polytech.ProjetLOO.DAO;

import fr.polytech.ProjetLOO.Model.Employe;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeDAO {
    public static Employe getEmploye(int id_client){
        SQLConnector connector = SQLConnector.getInstance();
        Employe employe;
        PreparedStatement ps = connector.getPreparedStatement("select nom , prenom , telephone , type_employe from employe where id_employe = ?");
        try {
            ps.setInt(1 , id_client);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                employe = new Employe(id_client , rs.getString(1) , rs.getString(2 ), rs.getString(3) , rs.getString(4));
            }else{
                employe = null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return  employe;
    }
}
