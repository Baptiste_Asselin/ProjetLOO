package fr.polytech.ProjetLOO.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SQLConnector {

    private static SQLConnector instance;

    private Connection connection;

    private SQLConnector() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch(ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        }
    }

    public static SQLConnector getInstance() {
        if(instance == null) {
            instance = new SQLConnector();
        }
        return instance;
    }

    public PreparedStatement getPreparedStatement(String sql) {
        if(connection == null) {
            try {
                connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/projetloo","admin","admin");
                return connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        try {
            return connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
        } catch(SQLException se) {
            se.printStackTrace();
        }
        return null;

    }

    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }
        connection = null;
    }
}
