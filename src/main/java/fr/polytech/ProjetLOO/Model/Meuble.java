package fr.polytech.ProjetLOO.Model;

public class Meuble {
    private String code;
    private float prix;
    private float hauteur;
    private float largeur;
    private float profondeur;

    public String getCode() {
        return code;
    }

    public Meuble(String code, float prix, float hauteur, float largeur, float profondeur) {
        this.code = code;
        this.prix = prix;
        this.hauteur = hauteur;
        this.largeur = largeur;
        this.profondeur = profondeur;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public float getHauteur() {
        return hauteur;
    }

    public void setHauteur(float hauteur) {
        this.hauteur = hauteur;
    }

    public float getLargeur() {
        return largeur;
    }

    public void setLargeur(float largeur) {
        this.largeur = largeur;
    }

    public float getProfondeur() {
        return profondeur;
    }

    public void setProfondeur(float profondeur) {
        this.profondeur = profondeur;
    }

    @Override
    public String toString() {
        return "Meuble{" +
                "code=" + code +
                ", prix=" + prix +
                ", hauteur=" + hauteur +
                ", largeur=" + largeur +
                ", profondeur=" + profondeur +
                '}';
    }
}
