package fr.polytech.ProjetLOO.Model;

import java.util.List;

public class Meuble_Contrat {
    private String code;
    private  int id_contrat;
    private int quantite;

    private int idClient;

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public Meuble_Contrat(String code, int quantite , int id_contrat) {
        this.code = code;
        this.quantite = quantite;
        this.id_contrat = id_contrat;
    }

    public int getId_contrat() {
        return id_contrat;
    }

    public void setId_contrat(int id_contrat) {
        this.id_contrat = id_contrat;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    @Override
    public String toString() {
        return "Meuble_Contrat{" +
                "code='" + code + '\'' +
                ", id_contrat=" + id_contrat +
                ", quantite=" + quantite +
                '}';
    }

}
