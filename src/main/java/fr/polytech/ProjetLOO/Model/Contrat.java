package fr.polytech.ProjetLOO.Model;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class Contrat {
    private int id;
    private LocalDate date_conclusion;
    private String adresse_livraison;
    private String description;
    private float prix;
    private List<Meuble_Contrat> meubles;
    private List<Livraison> livraisons;

    public List<Meuble_Contrat> getMeubles() {
        return meubles;
    }

    public void setMeubles(List<Meuble_Contrat> meubles) {
        this.meubles = meubles;
    }

    public List<Livraison> getLivraisons() {
        return livraisons;
    }

    public void setLivraisons(List<Livraison> livraisons) {
        this.livraisons = livraisons;
    }

    private int idClient;

    private int idEmploye;

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public int getIdEmploye() {
        return idEmploye;
    }

    public void setIdEmploye(int idEmploye) {
        this.idEmploye = idEmploye;
    }

    public Contrat(int id, LocalDate date_conclusion, String adresse_livraison, String description, float prix) {
        this.id = id;
        this.date_conclusion = date_conclusion;
        this.adresse_livraison = adresse_livraison;
        this.description = description;
        this.prix = prix;
        this.meubles = new LinkedList<Meuble_Contrat>();
        this.livraisons = new LinkedList<Livraison>();

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getDate_conclusion() {
        return date_conclusion;
    }

    public void setDate_conclusion(LocalDate date_conclusion) {
        this.date_conclusion = date_conclusion;
    }

    public String getAdresse_livraison() {
        return adresse_livraison;
    }

    public void setAdresse_livraison(String adresse_livraison) {
        this.adresse_livraison = adresse_livraison;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }
    public void addMeuble(Meuble_Contrat meuble){
        meubles.add(meuble);
    }
    public void addLivraison(Livraison livraison){
        livraisons.add(livraison);
    }

    @Override
    public String toString() {
        return "Contrat{" +
                "id=" + id +
                ", date_conclusion=" + date_conclusion +
                ", adresse_livraison='" + adresse_livraison + '\'' +
                ", description='" + description + '\'' +
                ", prix=" + prix +
                ", meubles=" + meubles +
                ", livraisons=" + livraisons +
                '}';
    }
}
