package fr.polytech.ProjetLOO.Model;

public class Employe {
    private int id_employe;
    private String nom;
    private String prenom;
    private String telephone;
    private String type_employe;

    public Employe(int id_employe, String nom, String prenom, String telephone, String type_employe) {
        this.id_employe = id_employe;
        this.nom = nom;
        this.prenom = prenom;
        this.telephone = telephone;
        this.type_employe = type_employe;
    }

    public int getId_employe() {
        return id_employe;
    }

    public void setId_employe(int id_employe) {
        this.id_employe = id_employe;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getType_employe() {
        return type_employe;
    }

    public void setType_employe(String type_employe) {
        this.type_employe = type_employe;
    }
}
