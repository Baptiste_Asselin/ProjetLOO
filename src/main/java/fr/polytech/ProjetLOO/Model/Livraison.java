package fr.polytech.ProjetLOO.Model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

public class Livraison {
    private int id_livraison;
    private LocalDate date_livraison;
    private LocalTime heure_livraison;
    //private int id_contrat;
    private List<Meuble_livree> meubles;

    public List<Meuble_livree> getMeubles() {
        return meubles;
    }

    public void setMeubles(List<Meuble_livree> meubles) {
        this.meubles = meubles;
    }

    public Livraison(int id_livraison, LocalDate date_livraison, LocalTime heure_livraison /*, int id_contrat*/) {
        this.id_livraison = id_livraison;
        this.date_livraison = date_livraison;
        this.heure_livraison = heure_livraison;
        this.meubles = new LinkedList<Meuble_livree>();
        //this.id_contrat = id_contrat;
    }

    public int getId_livraison() {
        return id_livraison;
    }

    public void setId_livraison(int id_livraison) {
        this.id_livraison = id_livraison;
    }

    public LocalDate getDate_livraison() {
        return date_livraison;
    }

    public void setDate_livraison(LocalDate date_livraison) {
        this.date_livraison = date_livraison;
    }

    public LocalTime getHeure_livraison() {
        return heure_livraison;
    }

    public void setHeure_livraison(LocalTime heure_livraison) {
        this.heure_livraison = heure_livraison;
    }

    /*public int getId_contrat() {
        return id_contrat;
    }

    public void setId_contrat(int id_contrat) {
        this.id_contrat = id_contrat;
    }*/
    public void addMeuble(Meuble_livree meuble){
        this.meubles.add(meuble);
    }

    @Override
    public String toString() {
        return "Livraison{" +
                "id_livraison=" + id_livraison +
                ", date_livraison=" + date_livraison +
                ", heure_livraison=" + heure_livraison +
                ", meubles=" + meubles +
                '}';
    }
}
