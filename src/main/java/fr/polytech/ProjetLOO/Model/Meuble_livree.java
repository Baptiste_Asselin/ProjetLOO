package fr.polytech.ProjetLOO.Model;

public class Meuble_livree {
    private String code;
    private int id_livraison;
    private int quantite ;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getId_livraison() {
        return id_livraison;
    }

    public void setId_livraison(int id_livraison) {
        this.id_livraison = id_livraison;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public Meuble_livree(String code, int id_livraison, int quantite) {
        this.code = code;
        this.id_livraison = id_livraison;
        this.quantite = quantite;
    }

    @Override
    public String toString() {
        return "Meuble_livree{" +
                "code='" + code + '\'' +
                ", id_livraison=" + id_livraison +
                ", quantite=" + quantite +
                '}';
    }
}
