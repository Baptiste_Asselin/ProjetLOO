package fr.polytech.ProjetLOO.Model;

import java.util.Collections;
import java.util.List;

public class Entreprise {
    private String name;

    private String address;

    private List<String> number;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getNumber() {
        return Collections.unmodifiableList(number);
    }

    public void setNumber(List<String> number) {
        this.number = number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Entreprise(String name, String address) {
        this.name = name;
        this.address = address;
    }
}
