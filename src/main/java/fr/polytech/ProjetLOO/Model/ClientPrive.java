package fr.polytech.ProjetLOO.Model;

public class ClientPrive {
    private String name;
    private String firstname;
    private String phone;
    private String adresse;

    public ClientPrive(String name, String firstname, String phone, String adresse) {
        this.name = name;
        this.firstname = firstname;
        this.phone = phone;
        this.adresse = adresse;
    }

    @Override
    public String toString() {
        return "ClientPrive{" +
                "name='" + name + '\'' +
                ", firstname='" + firstname + '\'' +
                ", phone='" + phone + '\'' +
                ", adresse='" + adresse + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
}
